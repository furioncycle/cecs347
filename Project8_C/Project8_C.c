/* Angel Marquez
	 CECS 347
	 Project 8
	 5/9/13
*/
#include "Lpc214x.h"
#include <stdio.h>
#include <stdlib.h>
void Config_PLL(void);
void Config_MAM(void);
void memoryPins(void);
void putChar(char Charc);
void Config_UART1(void);
int getChar(void);
void getPrompt(void);
void putLine(char *p);
void putChar1(int ch);
int value;
int hexValue;
int counter=0;
int main(void){
		int value;
		Config_PLL();
		Config_MAM();
		memoryPins();
		Config_UART1();
		getPrompt();
		while(1){
			value= getChar();
			//Check for CR & delete
			if(value==0x0D){
				putChar('\n');
				getPrompt();
			}
			else if(value==0x08){
				if(counter!=0){
					putChar1(0x08);
					putChar1(0x20);
					putChar1(0x08);
				}
			}
			else{
				putChar(value);
				counter++;
			}
		}
		
		
}
void Config_PLL(void){
	PLL0CFG= 0x00000023;	//sets the cclk to 48 MHz
	PLL0CON= 0x00000001;
	
	PLL0FEED= 0x000000AA;	//update PLL register  with feed sequence
	PLL0FEED= 0x00000055;
	
	while(!(PLL0STAT & 0x00000400));	//test lock bit
	
	PLL0CON = 0x00000003; //connect the PLL 
	
	PLL0FEED= 0x000000AA;	//update the PLL register
	PLL0FEED= 0x00000055;
	
	VPBDIV= 0x00000002; //sets  VPB clock = cpu clock/2
}
void Config_MAM(void){
		MAMTIM= 0x0000003; //Fetch cycle is 3 cclk
}
void memoryPins(void){
		PINSEL0= 0x00050000; //enable the RxD and TxD pins for UART1
		PINSEL1= 0x00000000;
		PINSEL2= 0x0E6149E4;	 
}
void Config_UART1(void){
		U1LCR= 0x000000B3; //8 bits no parity bit and 1 stop bit
		U1DLL= 0x0000001A; //For a baud rate of 57600
		U1LCR= 0x00000003; //DLAB=0
}
void putChar(char Charc){

 		while(!(U1LSR & 0x20));
		U1THR=Charc;
}
void putChar1(int ch){
		while(!(U1LSR & 0x20));
		U1THR=ch;
}


int getChar(void){
	
		while(!(U1LSR & 0x01));
		value = U1RBR;
		return (value);
		
}

void getPrompt(void){
	 char *prompt = "Lets write in C:\0";	
	 putLine(prompt);
}

void putLine(char *p){
	while(*p!= NULL){
			putChar(*p);
		  p++;
	 }
}


	


