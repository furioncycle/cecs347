;Angel Marquez
;CECS 347
;02/18/13
;Lab 1
;Description: Make a sorting algorithm in assembly
;-----------------------------------------------------------------------------------
;----------------------------Selection Sort-----------------------------------------
; for(j=0;j<n-1;j++)
;	min=j;
;		for(i=j+1; i<n; i++)
;			if(a[i] <a[min]
;				min=i;
;        if(min!=j)
;			swap
;what the code should some what be like taken from wiki
SRAM  EQU 0x40000000
AMT32 EQU 0x20			;amount of values in the table


;This is our starting memory location for SRAM
;The AREA directive defines a block of data or code
; The CODE directive allows the program to use machine instruction
; The READONLY directive indicates that this section should not be written to.
		 AREA SelectionSort, CODE, READONLY
			
;Declares an entry point to this program.
         ENTRY ; mark first instruction to execute

		
		 LDR R0, =table 			    ;pointer to the start of the table
		 LDR R1, =SRAM         	;pointer to the memory location 
loop_t		 
		 LDRB R2, [R0], #1			;load from table to temp
		 STRB R2, [R1], #1			;store into SRAM
		 CMP  R1, #SRAM+AMT32		;is the table finished putting all the values 
									;into the SRAM
		 BNE  loop_t				;continue to load into SRAM until finished
		 LDR  R3, =SRAM+AMT32		;load the size of the table into a temp reg
		 LDR  R4, =SRAM				;load the initial of the temp register
first_for
		 MOV  R5, R4				;Minimum value location
		 ADD  R1, R4, #1			;To get the next value above it
sec_for
		 LDRB R6, [R1]				;load value from SRAM
		 LDRB R7, [R5]				;load initial value
		 CMP  R6, R7				;compare if  R7 < R6
		 MOVMI R5, R1				;if R7 < R6 move pointer location into R5 becomes new min value
		 
		 ADD  R1, R1, #1			;increment the pointer of memory location for loop
		 CMP  R1, R3				;Did we reach the end of the table
		 BMI  sec_for				;less than
									;if not then swap the values
		 LDRB  R6, [R4]				;swap and store	
		 LDR   R7, [R5]				
		 STRB  R7, [R4]
		 STRB  R6, [R5]		
		 
		 ADD   R4, R4, #1			;inc pointer
		 CMP   R4, #SRAM+AMT32-1	;Compare with the location to the last location of the table
		 BCC   first_for
		 

stop     B stop

table 
		 DCB 0x0A, 0x01, 0x20, 0x3D, 0x4E, 0x05F, 0x10 ;7
		 DCB 0x00, 0x34, 0xFE, 0xCF, 0xD4, 0xC0, 0x9e ;7
		 DCB 0xB4, 0xFF, 0x90, 0xCC, 0x4F, 0x3B, 0x04 ; 7 
		 DCB 0x5E, 0x32, 0x19, 0x01, 0xD3, 0xC9, 0xEE ; 7
		 DCB 0xBB, 0x31, 0x97, 0x1A ; 4
		 END ; This designates the end of the source file