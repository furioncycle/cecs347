;/*****************************************************************************/
;/* STARTUP.S: Startup file for Philips LPC2000                               */
;/*****************************************************************************/
;/* Modified by John Tramel CSULB December 10, 2012                           */
;/*  this file should be used as the starting point when writing              */
;/*  the assembly code for CECS347.                                           */
;/*****************************************************************************/
;/* This file is part of the uVision/ARM development tools.                   */
;/* Copyright (c) 2005-2007 Keil Software. All rights reserved.               */
;/* This software may only be used under the terms of a valid, current,       */
;/* end user licence from KEIL for a compatible version of KEIL software      */
;/* development tools. Nothing else gives you the right to use this software. */
;/*****************************************************************************/
;/* Angel Marquez	 														  */
;/*			 edited: 2/21/2013				         			              */
;/*  				 changed VPBDIV file from 0x0 to 0x02 	     			  */
;/*					 changed PLL		 from 0x0 to 0x03   				  */
;/*					 changed MAM 		 0x02/0x03							  */
;/*					 enable the UART										  */
;/*			 edited: 3/18/13								         		  */  
;/					 Changed VPBDIV		from 0x02 to 0x1 for no divisor		  */
;/*					 Changed DLL/DLM	DLL=0x86 DLM=0x01 to give both		  */
;/*										the Pclk and Processor 60MHz		  */
;/*			 edited: 3/19/13										          */
;/*					 Added Recieve Method									  */
;/*					 Added Various Parameters for ASCII						  */
;/*			 edited: 4/3/13
;/*					 Enabled the External Interrupt 2 in the UART config.     */
;/*				     0x00005C00000											  */
;/*****************************************************************************/

; *********************************************************************
; Standard definitions of Mode bits and Interrupt (I & F) flags in PSRs
; *********************************************************************

Mode_USR        EQU     0x10
Mode_FIQ        EQU     0x11
Mode_IRQ        EQU     0x12
Mode_SVC        EQU     0x13
Mode_ABT        EQU     0x17
Mode_UND        EQU     0x1B
Mode_SYS        EQU     0x1F

I_Bit           EQU     0x80            ; when I bit is set, IRQ is disabled
F_Bit           EQU     0x40            ; when F bit is set, FIQ is disabled

; *********************************************************************
; Setup the stack for each of the modes of the ARM7
; *********************************************************************

;// <h> Stack Configuration (Stack Sizes in Bytes)
;//   <o0> Undefined Mode      <0x0-0xFFFFFFFF:8>
;//   <o1> Supervisor Mode     <0x0-0xFFFFFFFF:8>
;//   <o2> Abort Mode          <0x0-0xFFFFFFFF:8>
;//   <o3> Fast Interrupt Mode <0x0-0xFFFFFFFF:8>
;//   <o4> Interrupt Mode      <0x0-0xFFFFFFFF:8>
;//   <o5> User/System Mode    <0x0-0xFFFFFFFF:8>
;// </h>

UND_Stack_Size  EQU     0x00000000
SVC_Stack_Size  EQU     0x00000008
ABT_Stack_Size  EQU     0x00000000
FIQ_Stack_Size  EQU     0x00000000
IRQ_Stack_Size  EQU     0x00000080
USR_Stack_Size  EQU     0x00000400

ISR_Stack_Size  EQU     (UND_Stack_Size + SVC_Stack_Size + ABT_Stack_Size + \
                         FIQ_Stack_Size + IRQ_Stack_Size)

                AREA    STACK, NOINIT, READWRITE, ALIGN=3

Stack_Mem       SPACE   USR_Stack_Size
__initial_sp    SPACE   ISR_Stack_Size

Stack_Top

; *********************************************************************
; Setup the VPBDIV for the Peripheral Clock
; *********************************************************************

VPBDIV          EQU     0xE01FC100      ; VPBDIV Address

;// <e> VPBDIV Setup
;// <i> Peripheral Bus Clock Rate
;//   <o1.0..1>   VPBDIV: VPB Clock
;//               <0=> VPB Clock = CPU Clock / 4
;//               <1=> VPB Clock = CPU Clock
;//               <2=> VPB Clock = CPU Clock / 2
;//   <o1.4..5>   XCLKDIV: XCLK Pin
;//               <0=> XCLK Pin = CPU Clock / 4
;//               <1=> XCLK Pin = CPU Clock
;//               <2=> XCLK Pin = CPU Clock / 2
;// </e>
VPBDIV_SETUP    EQU     1  
VPBDIV_Val      EQU     0x00000001

; *********************************************************************
; Setup the PLL for establishing the CPU clock frequency
; *********************************************************************

; Phase Locked Loop (PLL) definitions
PLL_BASE        EQU     0xE01FC080      ; PLL Base Address
PLLCON_OFS      EQU     0x00            ; PLL Control Offset
PLLCFG_OFS      EQU     0x04            ; PLL Configuration Offset
PLLSTAT_OFS     EQU     0x08            ; PLL Status Offset
PLLFEED_OFS     EQU     0x0C            ; PLL Feed Offset
PLLCON_PLLE     EQU     (1<<0)          ; PLL Enable
PLLCON_PLLC     EQU     (1<<1)          ; PLL Connect
PLLCFG_MSEL     EQU     (0x1F<<0)       ; PLL Multiplier
PLLCFG_PSEL     EQU     (0x03<<5)       ; PLL Divider
PLLSTAT_PLOCK   EQU     (1<<10)         ; PLL Lock Status

;// <e> PLL Setup
;//   <o1.0..4>   MSEL: PLL Multiplier Selection
;//               <1-32><#-1>
;//               <i> M Value
;//   <o1.5..6>   PSEL: PLL Divider Selection
;//               <0=> 1   <1=> 2   <2=> 4   <3=> 8
;//               <i> P Value
;// </e>
PLL_SETUP       EQU     1			;changed from 0
PLLCFG_Val      EQU     0x00000024 ;changed this from 0x00000000

; *********************************************************************
; Setup the memory accelerator module to enhance memory performance
; *********************************************************************

; Memory Accelerator Module (MAM) definitions
MAM_BASE        EQU     0xE01FC000      ; MAM Base Address
MAMCR_OFS       EQU     0x00            ; MAM Control Offset
MAMTIM_OFS      EQU     0x04            ; MAM Timing Offset

;// <e> MAM Setup
;//   <o1.0..1>   MAM Control
;//               <0=> Disabled
;//               <1=> Partially Enabled
;//               <2=> Fully Enabled
;//               <i> Mode
;//   <o2.0..2>   MAM Timing
;//               <0=> Reserved  <1=> 1   <2=> 2   <3=> 3
;//               <4=> 4         <5=> 5   <6=> 6   <7=> 7
;//               <i> Fetch Cycles
;// </e>
MAM_SETUP       EQU     1 ;changed from 0
MAMCR_Val       EQU     0x00000002 ;from 00000000
MAMTIM_Val      EQU     0x00000003 ;0x00000000
;page 49

; *********************************************************************
; Setup the pinselect IO selection
; *********************************************************************

; External Memory Pins definitions
PINSEL0         EQU     0xE002C000      ; PINSEL0 Address
PINSEL0_Val     EQU     0x00000000
PINSEL1         EQU     0xE002C004      ; PINSEL1 Address
PINSEL1_Val     EQU     0x00000000
PINSEL2         EQU     0xE002C014      ; PINSEL2 Address
PINSEL2_Val     EQU     0x0E6149E4      ; CS0..3, OE, WE, BLS0..3, 
                                        ; D0..31, A2..23, JTAG Pins
				
;**********************************************************************
; UART0 INITIALIZATION
;**********************************************************************

U0RBR			EQU		0xE000C000	;Reciever Buffer register
U0THR			EQU		0xE000C000	;Transmit Holding register
U0DLL			EQU		0xE000C004	;Divisor Latch LSB			;Divisor=pclk/(16*baud)
U0DLM			EQU		0xE000C004	;Divisor Latch MSB
;U0IER			EQU		0xE000C004	;Interrupt Enable register
;U0IIR			EQU		0xE000C008	;Interrupt ID reg
;U0FCR			EQU		0xE000C008	;FIFO Control register
;Line Control register
;Bits[1:0]: Word Length: 00: 5-bits, 01: 6-bits, 10: 7-bits, 11: 8-bits
;Bits[2]: Stop Bit select: 0:1 stop bit, 1:2 stop bit
;Bit[3]: Parity Enabled: 0: disabled, 1:Enabled
;Bits[5:4]: Parity Select: 00: Odd, 01: Even, 10: Forced 1, 11: Forced 0
;Bit[6]: Break Control: 0: Disable Break trans, 1: Enable Break trans
;Bit[7]: Divisor Latch Acess Bit(DLAB) 
U0LCR			EQU		0xE000C00C	
U0LSR			EQU		0xE000C014	;Line Status register
;U0SCR			EQU		0xE000C01C	;Scratch Pad register
;U0ACR			EQU		0xE000C020	;Auto-baud Control
;U0FDR			EQU		0xE000C028	;Fractional Divider register
U0TER			EQU		0xE000C030	;Tx. Enable Register
;**********************************************************************
; UART1 INITIALIZATION
;**********************************************************************
UART1_addr		EQU		0xE0010000
U1RBR			EQU		0xE0010000	;Reciever Buffer register
U1THR			EQU		0xE0010000	;Transmit Holding register
U1DLL			EQU		0xE0010000	;Divisor Latch LSB
U1DLM			EQU		0xE0010004	;Divisor Latch MSB
;U1IER			EQU		0xE0010004	;Interrupt Enable register
;U1IIR			EQU		0xE0010008	;Interrupt ID reg
;U1FCR			EQU		0xE0010008	;FIFO Control register
U1LCR			EQU		0xE001000C	;Line Control register
U1LSR			EQU		0xE0010014	;Line Status register
;U1SCR			EQU		0xE001001C	;Scratch Pad register
;U1ACR			EQU		0xE0010020	;Auto-baud Control
;U1FDR			EQU		0xE0010028	;Fractional Divider register
U1TER			EQU		0xE0010030	;Tx. Enable Register


				
; *********************************************************************
; Enter into the ARM codespace for starting the ARM7 processor
; *********************************************************************

                PRESERVE8

                AREA    RESET, CODE, READONLY
                ARM
                ENTRY

; *********************************************************************
; Exception vectors for the ARM7
; *********************************************************************

Vectors         LDR     PC, Reset_Addr         
                LDR     PC, Undef_Addr
                LDR     PC, SWI_Addr
                LDR     PC, PAbt_Addr
				LDR     PC, DAbt_Addr
                NOP                            ; Reserved Vector 
;               LDR     PC, IRQ_Addr
                LDR     PC, [PC, #-0x0FF0]     ; Vector from VicVectAddr
                LDR     PC, FIQ_Addr

Reset_Addr      DCD     Reset_Handler
Undef_Addr      DCD     Undef_Handler
SWI_Addr        DCD     SWI_Handler
PAbt_Addr       DCD     PAbt_Handler
DAbt_Addr       DCD     DAbt_Handler
                DCD     0                      ; Reserved Address 
IRQ_Addr        DCD     IRQ_Handler
FIQ_Addr        DCD     FIQ_Handler

; *********************************************************************
; Dummy handlers as stubs ready for remapping to actual routines
; *********************************************************************

Undef_Handler   B       Undef_Handler
SWI_Handler     B       SWI_Handler
PAbt_Handler    B       PAbt_Handler
DAbt_Handler    B       DAbt_Handler
IRQ_Handler     B       IRQ_Handler
FIQ_Handler     B       FIQ_Handler


; *********************************************************************
; Reset Handler - begin executing here on release of reset      
; *********************************************************************

                EXPORT  Reset_Handler
Reset_Handler   

; Setup VPBDIV
                IF      VPBDIV_SETUP <> 0
                LDR     R0, =VPBDIV
                LDR     R1, =VPBDIV_Val
                STR     R1, [R0]
                ENDIF

; Setup PLL
                IF      PLL_SETUP <> 0
                LDR     R0, =PLL_BASE
                MOV     R1, #0xAA
                MOV     R2, #0x55

;  Configure and Enable PLL
                MOV     R3, #PLLCFG_Val
                STR     R3, [R0, #PLLCFG_OFS] 
                MOV     R3, #PLLCON_PLLE
                STR     R3, [R0, #PLLCON_OFS]
                STR     R1, [R0, #PLLFEED_OFS]
                STR     R2, [R0, #PLLFEED_OFS]

;  Wait until PLL Locked
PLL_Loop        LDR     R3, [R0, #PLLSTAT_OFS]
                ANDS    R3, R3, #PLLSTAT_PLOCK
                BEQ     PLL_Loop

;  Switch to PLL Clock
                MOV     R3, #(PLLCON_PLLE:OR:PLLCON_PLLC)
                STR     R3, [R0, #PLLCON_OFS]
                STR     R1, [R0, #PLLFEED_OFS]
                STR     R2, [R0, #PLLFEED_OFS]
                ENDIF   ; PLL_SETUP

; Setup MAM
                IF      MAM_SETUP <> 0
                LDR     R0, =MAM_BASE
                MOV     R1, #MAMTIM_Val
                STR     R1, [R0, #MAMTIM_OFS] 
                MOV     R1, #MAMCR_Val
                STR     R1, [R0, #MAMCR_OFS] 
                ENDIF   ; MAM_SETUP

; Setup Stack for each mode
                LDR     R0, =Stack_Top

;  Enter Undefined Instruction Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_UND:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #UND_Stack_Size

;  Enter Abort Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_ABT:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #ABT_Stack_Size

;  Enter FIQ Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_FIQ:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #FIQ_Stack_Size

;  Enter IRQ Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_IRQ:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #IRQ_Stack_Size

;  Enter Supervisor Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #SVC_Stack_Size

;  Enter User Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_USR
                IF      :DEF:__MICROLIB

                EXPORT __initial_sp

                ELSE

                MOV     SP, R0
                SUB     SL, SP, #USR_Stack_Size

                ENDIF
;------------------------------------------------------------
;				VIC 
;------------------------------------------------------------
VICCONFIG
VICIntSelect	EQU		0xFFFFF00C
VICIntEnable	EQU		0xFFFFF010
VICIntEnClr		EQU		0xFFFFF014
VICSoftInt		EQU		0xFFFFF018
VICSoftIntClear	EQU		0xFFFFF01C
VICVectAddr		EQU		0xFFFFF030
VICVectAddr0	EQU		0xFFFFF100
VICVectAddr1 	EQU		0xFFFFF104
VICVectCntl0	EQU		0xFFFFF200
VICVectCntl1	EQU		0xFFFFF204
				LDR		R5, =VICIntSelect	;Make UART and EXT2 IRQ
				MOV 	R6, #0x00000000
				STRB	R6, [R5]
				LDR		R5, =VICIntEnable
				MOV		R6, #0x00001000 	;Not letting me get the external on
				STRB	R6, [R5]
				MOV 	R6, #0x0080
				STRB	R6, [R5]
				LDR		R5, =VICVectAddr0
				MOV 	R6, #0x00000010
				STRB	R6, [R5]
				LDR		R5, =VICVectAddr1
				MOV		R6, #0x00000020
				STRB	R6, [R5]
				LDR		R5, =VICVectCntl0
				MOV 	R6, #0x00000027
				STRB	R6, [R5]
				LDR		R5, =VICVectCntl1
				MOV		R6, #0x00000030
				STRB	R6, [R5]
;-------------------------------------------------------------
;				UART
;-------------------------------------------------------------
UARTCONFIG
LCR1			EQU		0x0c
LSR1			EQU		0x14
DLM				EQU		0x04
URBR1			EQU		0xE0010000
UTHR1			EQU		0xE0010000
UART0_addr		EQU		0xE000C000
RAMSTART		EQU 	0x40000000
					LDR		R5, =PINSEL0			;to the stack and then increments
				LDR 	R6, [R5]
				BIC 	R6, R6, #0xFFFFFFFF		;clear the lower nibble
				ORR		R6, R6, #0x0005C000		;set P0.8 to Tx1(UART1) & Rx1 (UART1)
				
				STR		R6, [R5]				
				LDR		R5, =UART1_addr			;Start of the UART1 address
				MOV		R6, #0x8B			;Set 8 bit, Odd Parity, 1 stop bit
				STRB	R6, [R5, #LCR1]			
				MOV		R6, #0x86				;set the baud rate to 9600 and
				LDR		R5, =UART1_addr
				STRB	R6, [R5]				;
				LDR		R5, =UART1_addr+DLM
				MOV		R6, #0x01				
				STRB	R6, [R5]			;upper divisor latch
				LDR 	R5, =UART1_addr
				MOV 	R6, #0x3
				STRB	R6, [R5, #LCR1]			;Tx and Rx buffer is setup
				LDR		R5, =UART1_addr+DLM
				MOV		R6, #0x07
				STRB	R6, [R5]
; *********************************************************************
; Begin your code here. After initialization of LPC2148.        
; *********************************************************************	
				LDR		R1, =PROMPT_D	;load starting address of prompt table
				LDR 	R2, =RAMSTART	;memory location of SRAM
				MOV		R0, #0
				BL		PROMPT
HERE		
				BL		RECIEVE
				ADD		R0, #1				
				CMP		R0, #41				
				BLEQ	Forty_newLine
				CMP		R4, #0x7E			;Was there a tilda??
				BEQ		DUMP
				CMP		R4, #0x08			;Did they backspace
				BLEQ	BACKSPACE 
				CMP		R4, #0x2A			;Did they press an asterisk
				BLEQ	HOME
				CMP		R4, #0x0D			;Did they input a carriage return
				BLEQ	NEWLINE
				BL		TRANSMIT
				B		HERE
PROMPT
				STMIA	sp!,{R5, R6,lr}
				LDRB	R4, [R1],#1
				CMP 	R4, #0
				BLNE	TRANSMIT
				BNE		PROMPT
				LDMDB	sp!,{R5,R6,pc}
RECIEVE			
				STMIA	sp!,{R5,R6,lr}
wait_Rec		LDRB	R6, [R5,#LSR1]
				ANDS	R6,R6, #0x01
				BEQ		wait_Rec
				LDRB	R4, [R5]
				STRB	R4, [R2],#1			;store character in ram location
				LDMDB 	sp!, {R5,R6,pc}
TRANSMIT		
				STMIA	sp!,{R5,R6,lr}
				LDR 	R5, =UTHR1
wait_Tran		LDRB	R6, [R5, #LSR1]
				ANDS	R6, R6, #0x20
				BEQ		wait_Tran
				STRB	R4, [R5]
				LDMDB	sp!,{R5,R6,pc}
Forty_newLine	
				STMIA	sp!, {R5,R6,lr}
				MOV		R4, #0x0D			;Transmit a newline
				BL 		TRANSMIT
				MOV		R4, #0x0A
				;BL		TRANSMIT
				MOV		R0, #0
				LDMDB	sp!, {R5,R6,pc}
NEWLINE	
				STMIA	sp!,{R5,R6,lr}
				MOV		R9, R4
				MOV 	R4, #0x0A			;Place a new line feed instead of carriage return
				BL		TRANSMIT
				MOV 	R4, R9
				BL 		TRANSMIT
				LDR 	R1, =PROMPT_D
				BL		PROMPT
				LDMDB	sp!,{R5,R6,pc}	
BACKSPACE		
				STMIA	sp!,{R5,R6,lr}
				BL		TRANSMIT
				MOV		R4, #0x20			;Space bar
				BL		TRANSMIT
				MOV		R4, #0x08			;backspace again to go to the place
				SBC		R0, #3
				;MOV		R0, #0
				LDMDB	sp!,{R5,R6,pc}
HOME
				STMIA	sp!,{R5,R6,lr}
				LDR		R1, =HOMETOWN
				BL		PROMPT
				LDMDB	sp!,{R5,R6,pc}
DUMP			
				MOV		R4, #0x0A
				STRB	R4, [R5]		;place dump on a new line
				LDR		R2, =RAMSTART
Cont_DUMP		LDRB	R4, [R2],#1
				BL		TRANSMIT
				CMP		R4, #0x7E		;back to the tilda
				BEQ		Initialize
				BNE		Cont_DUMP
Initialize		LDR 	R2, =RAMSTART	;memory location of SRAM
				B		HERE
PROMPT_D
				DCB "Here is what you typed: ", 0
HOMETOWN
				DCB	"LA PUENTE",0
				
                END

