;/*****************************************************************************/
;/* STARTUP.S: Startup file for Philips LPC2000                               */
;/*****************************************************************************/
;/* Modified by John Tramel CSULB December 10, 2012                           */
;/*  this file should be used as the starting point when writing              */
;/*  the assembly code for CECS347.                                           */
;/*****************************************************************************/
;/* This file is part of the uVision/ARM development tools.                   */
;/* Copyright (c) 2005-2007 Keil Software. All rights reserved.               */
;/* This software may only be used under the terms of a valid, current,       */
;/* end user licence from KEIL for a compatible version of KEIL software      */
;/* development tools. Nothing else gives you the right to use this software. */
;/*****************************************************************************/
;/* Angel Marquez	 														  */
;/*			 edited: 2/21/2013				         			              */
;/*  				 changed VPBDIV file from 0x0 to 0x02 	     			  */
;/*					 changed PLL		 from 0x0 to 0x03   				  */
;/*					 changed MAM 		 0x02/0x03							  */
;/*					 enable the UART										  */
;/*			 edited: 3/18/13								         		  */  
;/					 Changed VPBDIV		from 0x02 to 0x1 for no divisor		  */
;/*					 Changed DLL/DLM	DLL=0x86 DLM=0x01 to give both		  */
;/*										the Pclk and Processor 60MHz		  */
;/*			 edited: 3/19/13										          */
;/*					 Added Recieve Method									  */
;/*					 Added Various Parameters for ASCII						  */
;/*			 edited: 4/3/13 - 4/10/13										  */
;/*					 -Enabled the External Interrupt 2 in the UART config.     */
;/*				     0x00005C00000   										  */
;/*					 -Enabled the VIC for UART1 & External Interrupt 2		  */
;/*					 IRQ interrupt
;/*					 -Modified the Recieve to incorparate the various		  */ 
;/*					 various functions like deleteing and newline after 40	  */
;/*					 characters												  */
;/*					 -Added an External interrupt function					  */
;/*			edited:  4/23/13												  */
;/*					 -Added External from System control block				  */
;/*****************************************************************************/

; *********************************************************************
; Standard definitions of Mode bits and Interrupt (I & F) flags in PSRs
; *********************************************************************

Mode_USR        EQU     0x10
Mode_FIQ        EQU     0x11
Mode_IRQ        EQU     0x12
Mode_SVC        EQU     0x13
Mode_ABT        EQU     0x17
Mode_UND        EQU     0x1B
Mode_SYS        EQU     0x1F

I_Bit           EQU     0x80            ; when I bit is set, IRQ is disabled
F_Bit           EQU     0x40            ; when F bit is set, FIQ is disabled

; *********************************************************************
; Setup the stack for each of the modes of the ARM7
; *********************************************************************

;// <h> Stack Configuration (Stack Sizes in Bytes)
;//   <o0> Undefined Mode      <0x0-0xFFFFFFFF:8>
;//   <o1> Supervisor Mode     <0x0-0xFFFFFFFF:8>
;//   <o2> Abort Mode          <0x0-0xFFFFFFFF:8>
;//   <o3> Fast Interrupt Mode <0x0-0xFFFFFFFF:8>
;//   <o4> Interrupt Mode      <0x0-0xFFFFFFFF:8>
;//   <o5> User/System Mode    <0x0-0xFFFFFFFF:8>
;// </h>

UND_Stack_Size  EQU     0x00000000
SVC_Stack_Size  EQU     0x00000008
ABT_Stack_Size  EQU     0x00000000
FIQ_Stack_Size  EQU     0x00000000
IRQ_Stack_Size  EQU     0x00000080
USR_Stack_Size  EQU     0x00000400

ISR_Stack_Size  EQU     (UND_Stack_Size + SVC_Stack_Size + ABT_Stack_Size + \
                         FIQ_Stack_Size + IRQ_Stack_Size)

                AREA    STACK, NOINIT, READWRITE, ALIGN=3

Stack_Mem       SPACE   USR_Stack_Size
__initial_sp    SPACE   ISR_Stack_Size

Stack_Top

; *********************************************************************
; Setup the VPBDIV for the Peripheral Clock
; *********************************************************************

VPBDIV          EQU     0xE01FC100      ; VPBDIV Address

;// <e> VPBDIV Setup
;// <i> Peripheral Bus Clock Rate
;//   <o1.0..1>   VPBDIV: VPB Clock
;//               <0=> VPB Clock = CPU Clock / 4
;//               <1=> VPB Clock = CPU Clock
;//               <2=> VPB Clock = CPU Clock / 2
;//   <o1.4..5>   XCLKDIV: XCLK Pin
;//               <0=> XCLK Pin = CPU Clock / 4
;//               <1=> XCLK Pin = CPU Clock
;//               <2=> XCLK Pin = CPU Clock / 2
;// </e>
VPBDIV_SETUP    EQU     1  
VPBDIV_Val      EQU     0x00000001

; *********************************************************************
; Setup the PLL for establishing the CPU clock frequency
; *********************************************************************

; Phase Locked Loop (PLL) definitions
PLL_BASE        EQU     0xE01FC080      ; PLL Base Address
PLLCON_OFS      EQU     0x00            ; PLL Control Offset
PLLCFG_OFS      EQU     0x04            ; PLL Configuration Offset
PLLSTAT_OFS     EQU     0x08            ; PLL Status Offset
PLLFEED_OFS     EQU     0x0C            ; PLL Feed Offset
PLLCON_PLLE     EQU     (1<<0)          ; PLL Enable
PLLCON_PLLC     EQU     (1<<1)          ; PLL Connect
PLLCFG_MSEL     EQU     (0x1F<<0)       ; PLL Multiplier
PLLCFG_PSEL     EQU     (0x03<<5)       ; PLL Divider
PLLSTAT_PLOCK   EQU     (1<<10)         ; PLL Lock Status

;// <e> PLL Setup
;//   <o1.0..4>   MSEL: PLL Multiplier Selection
;//               <1-32><#-1>
;//               <i> M Value
;//   <o1.5..6>   PSEL: PLL Divider Selection
;//               <0=> 1   <1=> 2   <2=> 4   <3=> 8
;//               <i> P Value
;// </e>
PLL_SETUP       EQU     1			;changed from 0
PLLCFG_Val      EQU     0x00000024 ;changed this from 0x00000000

; *********************************************************************
; Setup the memory accelerator module to enhance memory performance
; *********************************************************************

; Memory Accelerator Module (MAM) definitions
MAM_BASE        EQU     0xE01FC000      ; MAM Base Address
MAMCR_OFS       EQU     0x00            ; MAM Control Offset
MAMTIM_OFS      EQU     0x04            ; MAM Timing Offset

;// <e> MAM Setup
;//   <o1.0..1>   MAM Control
;//               <0=> Disabled
;//               <1=> Partially Enabled
;//               <2=> Fully Enabled
;//               <i> Mode
;//   <o2.0..2>   MAM Timing
;//               <0=> Reserved  <1=> 1   <2=> 2   <3=> 3
;//               <4=> 4         <5=> 5   <6=> 6   <7=> 7
;//               <i> Fetch Cycles
;// </e>
MAM_SETUP       EQU     1 ;changed from 0
MAMCR_Val       EQU     0x00000002 ;from 00000000
MAMTIM_Val      EQU     0x00000003 ;0x00000000
;page 49

; *********************************************************************
; Setup the pinselect IO selection
; *********************************************************************

; External Memory Pins definitions
PINSEL0         EQU     0xE002C000      ; PINSEL0 Address
PINSEL0_Val     EQU     0x00000000
PINSEL1         EQU     0xE002C004      ; PINSEL1 Address
PINSEL1_Val     EQU     0x00000000
PINSEL2         EQU     0xE002C014      ; PINSEL2 Address
PINSEL2_Val     EQU     0x0E6149E4      ; CS0..3, OE, WE, BLS0..3, 
                                        ; D0..31, A2..23, JTAG Pins
				
;**********************************************************************
; UART0 Definitions
;**********************************************************************

U0RBR			EQU		0xE000C000	;Reciever Buffer register
U0THR			EQU		0xE000C000	;Transmit Holding register
U0DLL			EQU		0xE000C004	;Divisor Latch LSB			;Divisor=pclk/(16*baud)
U0DLM			EQU		0xE000C004	;Divisor Latch MSB
;U0IER			EQU		0xE000C004	;Interrupt Enable register
;U0IIR			EQU		0xE000C008	;Interrupt ID reg
;U0FCR			EQU		0xE000C008	;FIFO Control register
;Line Control register
;Bits[1:0]: Word Length: 00: 5-bits, 01: 6-bits, 10: 7-bits, 11: 8-bits
;Bits[2]: Stop Bit select: 0:1 stop bit, 1:2 stop bit
;Bit[3]: Parity Enabled: 0: disabled, 1:Enabled
;Bits[5:4]: Parity Select: 00: Odd, 01: Even, 10: Forced 1, 11: Forced 0
;Bit[6]: Break Control: 0: Disable Break trans, 1: Enable Break trans
;Bit[7]: Divisor Latch Acess Bit(DLAB) 
U0LCR			EQU		0xE000C00C	
U0LSR			EQU		0xE000C014	;Line Status register
;U0SCR			EQU		0xE000C01C	;Scratch Pad register
;U0ACR			EQU		0xE000C020	;Auto-baud Control
;U0FDR			EQU		0xE000C028	;Fractional Divider register
U0TER			EQU		0xE000C030	;Tx. Enable Register
;**********************************************************************
; UART1 Definitions
;**********************************************************************
UART1_addr		EQU		0xE0010000
URBR1			EQU		0xE0010000	;Reciever Buffer register
UTHR1			EQU		0xE0010000	;Transmit Holding register
U1DLL			EQU		0xE0010000	;Divisor Latch LSB
U1DLM			EQU		0xE0010004	;Divisor Latch MSB
;U1IER			EQU		0xE0010004	;Interrupt Enable register
;U1IIR			EQU		0xE0010008	;Interrupt ID reg
;U1FCR			EQU		0xE0010008	;FIFO Control register
U1LCR			EQU		0xE001000C	;Line Control register
U1LSR			EQU		0xE0010014	;Line Status register
;U1SCR			EQU		0xE001001C	;Scratch Pad register
;U1ACR			EQU		0xE0010020	;Auto-baud Control
;U1FDR			EQU		0xE0010028	;Fractional Divider register
U1TER			EQU		0xE0010030	;Tx. Enable Register
LCR1			EQU		0x0c
LSR1			EQU		0x14
DLM				EQU		0x04
UIER1			EQU		0xE0010008	;Interrupt ID reg	
;**********************************************************************
; VIC Definitions
;**********************************************************************
VICIntSelect	EQU		0xFFFFF00C
VICIntEnable	EQU		0xFFFFF010
VICIntEnClr		EQU		0xFFFFF014
VICSoftInt		EQU		0xFFFFF018
VICSoftIntClear	EQU		0xFFFFF01C
VICVectAddr		EQU		0xFFFFF030
VICVectAddr0	EQU		0xFFFFF100
VICVectAddr1 	EQU		0xFFFFF104
VICVectCntl0	EQU		0xFFFFF200
VICVectCntl1	EQU		0xFFFFF204
;**********************************************************************
; System Control Block//External Interrupt Definitions
;**********************************************************************
EXTINT			EQU		0xE01FC140			;External Interrupt Flag Register
INTWAKE			EQU		0xE01FC144			;Interrupt Wakeup Register
											;This will cause the processor to wake
											;up from power down mode.
EXTMODE			EQU		0xE01FC148			;External Interrupt Mode Register
											;Level-0     Edge-1
EXTPOLAR		EQU		0xE01FC14C			;This is check which level or edge 
											;type will interrupt
; *********************************************************************
; Enter into the ARM codespace for starting the ARM7 processor
; *********************************************************************

                PRESERVE8

                AREA    RESET, CODE, READONLY
                ARM
                ENTRY

; *********************************************************************
; Exception vectors for the ARM7
; *********************************************************************

Vectors         LDR     PC, Reset_Addr         
                LDR     PC, Undef_Addr
                LDR     PC, SWI_Addr
                LDR     PC, PAbt_Addr
				LDR     PC, DAbt_Addr
                NOP                            ; Reserved Vector 
;               LDR     PC, IRQ_Addrs
                LDR     PC, [PC, #-0x0FF0]     ; Vector from VicVectAddr
                LDR     PC, FIQ_Addr

Reset_Addr      DCD     Reset_Handler
Undef_Addr      DCD     Undef_Handler
SWI_Addr        DCD     SWI_Handler
PAbt_Addr       DCD     PAbt_Handler
DAbt_Addr       DCD     DAbt_Handler
                DCD     0                      ; Reserved Address 
IRQ_Addr        DCD     IRQ_Handler
FIQ_Addr        DCD     FIQ_Handler

; *********************************************************************
; Dummy handlers as stubs ready for remapping to actual routines
; *********************************************************************

Undef_Handler   B       Undef_Handler
SWI_Handler     B       SWI_Handler
PAbt_Handler    B       PAbt_Handler
DAbt_Handler    B       DAbt_Handler
IRQ_Handler     B       IRQ_Handler
FIQ_Handler     B       FIQ_Handler


; *********************************************************************
; Reset Handler - begin executing here on release of reset      
; *********************************************************************

                EXPORT  Reset_Handler
Reset_Handler   

; Setup VPBDIV
                IF      VPBDIV_SETUP <> 0
                LDR     R0, =VPBDIV
                LDR     R1, =VPBDIV_Val
                STR     R1, [R0]
                ENDIF

; Setup PLL
                IF      PLL_SETUP <> 0
                LDR     R0, =PLL_BASE
                MOV     R1, #0xAA
                MOV     R2, #0x55

;  Configure and Enable PLL
                MOV     R3, #PLLCFG_Val
                STR     R3, [R0, #PLLCFG_OFS] 
                MOV     R3, #PLLCON_PLLE
                STR     R3, [R0, #PLLCON_OFS]
                STR     R1, [R0, #PLLFEED_OFS]
                STR     R2, [R0, #PLLFEED_OFS]

;  Wait until PLL Locked
PLL_Loop        LDR     R3, [R0, #PLLSTAT_OFS]
                ANDS    R3, R3, #PLLSTAT_PLOCK
                BEQ     PLL_Loop

;  Switch to PLL Clock
                MOV     R3, #(PLLCON_PLLE:OR:PLLCON_PLLC)
                STR     R3, [R0, #PLLCON_OFS]
                STR     R1, [R0, #PLLFEED_OFS]
                STR     R2, [R0, #PLLFEED_OFS]
                ENDIF   ; PLL_SETUP

; Setup MAM
                IF      MAM_SETUP <> 0
                LDR     R0, =MAM_BASE
                MOV     R1, #MAMTIM_Val
                STR     R1, [R0, #MAMTIM_OFS] 
                MOV     R1, #MAMCR_Val
                STR     R1, [R0, #MAMCR_OFS] 
                ENDIF   ; MAM_SETUP

; Setup Stack for each mode
                LDR     R0, =Stack_Top

;  Enter Undefined Instruction Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_UND:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #UND_Stack_Size

;  Enter Abort Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_ABT:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #ABT_Stack_Size

;  Enter FIQ Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_FIQ:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #FIQ_Stack_Size

;  Enter IRQ Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_IRQ:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #IRQ_Stack_Size

;  Enter Supervisor Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_SVC:OR:I_Bit:OR:F_Bit
                MOV     SP, R0
                SUB     R0, R0, #SVC_Stack_Size

;  Enter User Mode and set its Stack Pointer
                MSR     CPSR_c, #Mode_USR
                IF      :DEF:__MICROLIB

                EXPORT __initial_sp

                ELSE

                MOV     SP, R0
                SUB     SL, SP, #USR_Stack_Size

                ENDIF
;**********************************************************************
; UART1 Configuration
;**********************************************************************
UARTCONFIG
				LDR		R5, =PINSEL0			;to the stack and then increments
				LDR 	R6, [R5]
				BIC 	R6, R6, #0xFFFFFFFF		;clear the lower nibble
				ORR		R6, R6, #0x0005C000		;set P0.8 to Tx1(UART1) & set P0.9 Rx1 (UART1) & set P0.7 to EXT2
				STR		R6, [R5]				
				LDR		R5, =UART1_addr			;Start of the UART1 address
				MOV		R6, #0x8B			;Set 8 bit, Odd Parity, 1 stop bit
				STR		R6, [R5, #LCR1]			
				MOV		R6, #0x86				;set the baud rate to 9600 and
				LDR		R5, =UART1_addr
				STR		R6, [R5]				;
				LDR		R5, =UART1_addr+DLM
				MOV		R6, #0x01				
				STR		R6, [R5]			;upper divisor latch
				LDR 	R5, =UART1_addr
				MOV 	R6, #0x3
				STR		R6, [R5, #LCR1]			
				LDR		R5, =UART1_addr+DLM
				MOV		R6, #0x01				; Rx buffer is setup
				STR		R6, [R5]
				LDR		R5, =UIER1				;Enable the Uart1 interrupt
				MOV 	R6, #0x01
				STR		R6, [R5]
;**********************************************************************
; VIC Initialization
;**********************************************************************
VICCONFIG

				LDR		R5, =VICIntSelect	;Make UART and EXT2 IRQ
				MOV 	R6, #0x00000000
				STR		R6, [R5]
				LDR		R5, =VICIntEnable
				LDR		R6, =0x00010080 	;enables both UART1 and External Interrupt	
				STR		R6, [R5]
				LDR		R5, =VICVectCntl0
				MOV 	R6, #0x00000027		;enable UART interrupt
				STR		R6, [R5]			;and store in slot 0 control
				LDR		R5, =VICVectCntl1
				MOV		R6, #0x00000030		;enable external interrupt
				STR		R6, [R5]			;and store in slot 1 control
				LDR		R5, =VICVectAddr0	
				LDR 	R6, =RECEIVE_ISR		;UART1 ISR  in address o
				STR		R6, [R5]
				LDR		R5, =VICVectAddr1 	;External interrupt in address 1
				LDR		R6, =ExtInt_ISR
				STR		R6, [R5]
; *********************************************************************
;						System Control Block-External Interrupt
;									initialization
; *********************************************************************
				;Make 	ExtInt2 edge triggered
				LDR		R5, =EXTMODE
				MOV		R6, #0x04
				STR		R6, [R5]
				;Make 	ExtInt2 high-level edge 
				LDR		R5, =EXTPOLAR
				MOV		R6, #0x04
				STR		R6, [R5]
; *********************************************************************
; Begin your code here. After initialization of LPC2148.        
; *********************************************************************	
RAMSTART		EQU		0x40000000
				MOV		R12, #0
				LDR		R1, =PROMPT_D	;load starting address of prompt table
				BL		PROMPT
				LDR 	R2, =RAMSTART	;memory location of SRAM	
				MOV		R7, #0x0		;place holder
wait			B		wait			;wait for interrupt to happen
; **************************************************************************
;*******************Function************************************************
;***************************************************************************
PROMPT
				STMIA	sp!,{R5, R6,lr}
				LDRB	R10, [R1],#1
				CMP 	R10, #0
				BLNE	TRANSMIT
				BNE		PROMPT
				LDMDB	sp!,{R5,R6,pc}
RECEIVE			
				STMIA	sp!, {R5,R6,lr}
				ADD		R12, #1				;increment counter for 40 count			
				CMP		R12, #39				
				STRB	R10, [R2], #1		;store in ram location
				BLEQ	Forty_newLine
				CMP		R10, #0x7E			;Was there a tilda??
				BLEQ	DUMP
				CMP		R10, #0x08			;Did they backspace
				BLEQ	BACKSPACE 
				CMP		R10, #0x2A			;Did they press an asterisk
				BLEQ	HOME
				CMP		R10, #0x0D			;Did they input a carriage return
				BLEQ	NEWLINE
				BL		TRANSMIT
				LDMDB	sp!,{R5,R6,pc}
				
TRANSMIT		
				STMIA	sp!,{R5,R6,lr}
				LDR 	R6, =UTHR1
wait_Tran		LDRB	R11, [R6, #LSR1]
				ANDS	R11, R11, #0x20
				BEQ		wait_Tran
				STRB	R10, [R6]
				LDMDB	sp!,{R5,R6,pc}
Forty_newLine	
				STMIA	sp!, {R5,R6,lr}
				MOV		R10, #0x0D			;Transmit a newline
				BL 		TRANSMIT
				MOV		R10, #0x0A
				MOV		R12, #0
				LDMDB	sp!, {R5,R6,pc}
NEWLINE	
				STMIA	sp!,{R5,R6,lr}
				MOV		R9, R10
				MOV 	R10, #0x0A			;Place a new line feed instead of carriage return
				BL		TRANSMIT
				MOV 	R10, R9
				BL 		TRANSMIT
				LDR 	R1, =PROMPT_D
				BL		PROMPT
				LDMDB	sp!,{R5,R6,pc}	
BACKSPACE		
				STMIA	sp!,{R5,R6,lr}
				BL		TRANSMIT
				MOV		R10, #0x20			;Space bar
				BL		TRANSMIT
				MOV		R10, #0x08			;backspace again to go to the place
				SBC		R12, #4
				LDMDB	sp!,{R5,R6,pc}
HOME
				STMIA	sp!,{R5,R6,lr}
				LDR		R1, =HOMETOWN
				BL		PROMPT
;				SUB		R12, #9
				LDMDB	sp!,{R5,R6,pc}
DUMP			
				STMIA	sp!,{R5,R6,lr}
				MOV 	R4, #0
				MOV		R10, #0x0A
				BL		TRANSMIT
				LDR		R2, =RAMSTART
Cont_DUMP		LDRB	R10, [R2],#1
				CMP		R10, #0x7E		;back to the tilda
				BEQ		Initialize
				BL		TRANSMIT
				BNE		Cont_DUMP
Initialize		
				MOV 	R10, #0x0A		;newLine
				LDR 	R2, =RAMSTART	;memory location of SRAMs
				LDMDB	sp!,{R5,R6,pc}
;***************************************************************
;				ISR
;***************************************************************
RECEIVE_ISR
				SUB		lr, lr, #4
				STMIA	sp!,{R0,R8,lr}
				LDR		R6, =URBR1
				LDRB	R10, [R6]			;read char
				BL 		RECEIVE
				MOV 	R8, #0x0
				LDR		R0, =VICVectAddr
				STR		R8, [R0]			;write near the end of ISR
				LDMDB 	sp!, {R0,R8,pc}^
ExtInt_ISR
				SUB		lr, lr, #4
				STMIA	sp!, {R5, R6, lr}
CONT_EXT		LDR		R6, =ExtMsg			;load message
				LDRB	R10, [R6], #1
				CMP 	R10, #0				;is it at the end
				BLNE	TRANSMIT
				BNE		CONT_EXT
				MOV		R8, #0x00			;clear vector address
				LDR		R0, =VICVectAddr
				STR		R8, [R0]
				LDR		R0, =EXTINT			;load external to clear flag
				MOV		R8, #0x04			;clear flag
				STR		R8, [R0]			
				LDMDB	sp!, {R5,R6,pc}^
;***************************************************************
;				Prompts
;***************************************************************
PROMPT_D
				DCB "Here is what you typed: ", 0
HOMETOWN
				DCB	"LA PUENTE",0
ExtMsg
				DCB	"You just hit the external interrupt", 0
				
                END

